package Kyrsovoi;


import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Result {
    private final Map<File, List<ResultItem>> matches = new HashMap<>();

    public Map<File, List<ResultItem>> getMatches() {
        return matches;
    }

    public Result() {
    }

    public void addMatches(File file, List<ResultItem> listResultItem) {
        if (!listResultItem.isEmpty()) {
            matches.put(file, listResultItem);
        }
    }

    public boolean isEmpty() {
        return matches.isEmpty();
    }
}

