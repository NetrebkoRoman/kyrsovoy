package Kyrsovoi;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
public class Dispatcher {
    private final Config config;
    private final Handler handler;
    private final List<View> views = new ArrayList<>();
    private final ViewResolver viewResolver = new ViewResolver();
    public Dispatcher(String[] args) throws ConfigException {
        this.config = new Config(args);
        this.handler = new Handler();
        this.views.add(new ConsoleView());
    }
    public void dispatch() throws DispatchException {
        File fileOrDirectory = config.getDestination();
        String text4Search = config.getText4Search();
        List<File> fileList = new ArrayList<>();
        Result result = new Result();
        if (fileOrDirectory.isFile()) {
            fileList.add(fileOrDirectory);
        } else if (fileOrDirectory.isDirectory()) {
            File[] files = fileOrDirectory.listFiles();
            if (files != null){
                for (File f : files) {
                    fileList.add(f);
                }
            }
        }
        for (File file : fileList) {
            List<ResultItem> resultItemList = handler.handle(file, text4Search);
            if (!resultItemList.isEmpty()) {
                result.addMatches(file, resultItemList);
            }
        }
        String output = viewResolver.resolve(result, text4Search);
        for (View view : views) {
            view.print(output);
        }
    }
}



