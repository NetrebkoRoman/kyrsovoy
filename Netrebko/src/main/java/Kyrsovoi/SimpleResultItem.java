package Kyrsovoi;

public class SimpleResultItem implements ResultItem {
    private final String line;
    private final int lineNumber;

    public SimpleResultItem(int lineNumber, String line) {
        this.line = line;
        this.lineNumber = lineNumber;
    }

    @Override
    public String getFormatted() {
        return lineNumber + " -> " + line;
    }
}
