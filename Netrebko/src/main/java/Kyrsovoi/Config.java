package Kyrsovoi;

import java.io.File;

public class Config {
    private File destination;
    private String text4Search;

    public File getDestination() {
        return destination;
    }

    public String getText4Search() {
        return text4Search;
    }

    public Config(String[] args) throws ConfigException {
        parseArgs(args);
    }

    private void parseArgs(String[] args) throws ConfigException {
        if (args.length < 2) {
            throw new ConfigException("Not enough arguments. Must be at least \"text to search\" and \"path to file\".");
        }
        text4Search = args[args.length - 2];
        if (text4Search == null || text4Search.trim().isEmpty()) {
            throw new ConfigException("Argument \"text to search\" is absent or empty.");
        }
        String filePath = args[args.length - 1];
        destination = new File(filePath);
        if (!destination.exists()) {
            throw new ConfigException("File or directory " + filePath + " is not exist.");
        }
    }
}


