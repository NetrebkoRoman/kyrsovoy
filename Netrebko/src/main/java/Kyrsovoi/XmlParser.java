package Kyrsovoi;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class XmlParser extends AbstractParser {
    public XmlParser() {
        super(List.of(".xml"));
    }

    @Override
    public List<ResultItem> parse(File file, String test4Search) throws DispatchException {
        List<ResultItem> resultItemList = new ArrayList<>();
        XMLInputFactory factory = XMLInputFactory.newFactory();
        try (InputStream is = new FileInputStream(file)){
            XMLStreamReader reader = factory.createXMLStreamReader(is);
            String tag = "";
            while (reader.hasNext()) {
                int event = reader.next();
                if (event == XMLStreamConstants.CHARACTERS && !reader.getText().trim().isEmpty()) {
                    String text = reader.getText();
                    if (text.contains(test4Search)) {
                        resultItemList.add(new XmlResultItem("<" + tag + "> " + text));
                    }
                }
                if (event == XMLStreamConstants.START_ELEMENT) {
                    tag = reader.getLocalName();
                }
            }
        } catch (Exception e) {
            throw new DispatchException(e.getMessage());
        }
        return resultItemList;
    }
}

