package Kyrsovoi;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class UsedFileReader {
    public UsedFileReader() {
    }

      List<String> readLine(File file) throws DispatchException {
        List<String> listOfLine = new ArrayList<>();
        try (BufferedReader bf = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = bf.readLine()) != null) {
                listOfLine.add(line);
            }
        } catch (Exception e) {
            throw new DispatchException(e.getMessage());
        }
        return listOfLine;
    }

}
