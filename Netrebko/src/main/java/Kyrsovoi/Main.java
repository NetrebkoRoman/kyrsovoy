package Kyrsovoi;

public class Main {
    public static void main(String[] args) throws ConfigException, DispatchException {
        Dispatcher dispatcher = new Dispatcher(args);
        dispatcher.dispatch();
    }
}

