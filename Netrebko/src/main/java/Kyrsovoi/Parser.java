package Kyrsovoi;

import java.io.File;
import java.util.List;

public interface Parser {

        List<String> getSupportedExtensions();
        boolean isSupportFile(String fileName);
        List<ResultItem> parse(File file, String test4Search) throws DispatchException;
    }


