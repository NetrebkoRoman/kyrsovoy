package Kyrsovoi;


import java.io.File;
import java.util.List;
import java.util.Map;

public class ViewResolver{
    public String resolve(Result result, String text4Search) {
        StringBuilder sb = new StringBuilder();
        sb.append("===========================================================\n");
        sb.append("Smart File Handler CLI\n");
        sb.append("===========================================================\n");
        if (result.isEmpty()) {
            sb.append("Nothing found.");
            return sb.toString();
        }
        Map<File, List<ResultItem>> map = result.getMatches();
        for (Map.Entry<File, List<ResultItem>> entry : map.entrySet()) {
            List<ResultItem> resultItemList = entry.getValue();
            sb.append("File name is " + entry.getKey().getName() + "\n");
            sb.append("Result for '" + text4Search + "'. Total matches " + resultItemList.size() + "\n");
            for (ResultItem line : resultItemList) {
                sb.append(line.getFormatted() + "\n");
            }
            sb.append("===========================================================\n");
        }
        return sb.toString();
    }
}

