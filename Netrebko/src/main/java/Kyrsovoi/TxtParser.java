package Kyrsovoi;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class TxtParser extends AbstractParser {
    private final UsedFileReader fileReader = new UsedFileReader();

    public TxtParser() {
        super(List.of(".txt"));
    }

    @Override
    public List<ResultItem> parse(File file, String test4Search) throws DispatchException {
        List<ResultItem> resultItemList = new ArrayList<>();
        List<String> fileLines = (List<String>) fileReader.readLine(file);
        for (int i = 0; i < fileLines.size(); i++) {
            String currentLine = fileLines.get(i);
            if (currentLine.contains(test4Search)) {
                resultItemList.add(new SimpleResultItem(i + 1, currentLine));
            }
        }
        return resultItemList;
    }
}

