package Kyrsovoi;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Handler {
    private final List<Parser> parsers = new ArrayList<>();

    public Handler() {
        parsers.add(new TxtParser());
        parsers.add(new XmlParser());
    }
    public List<ResultItem> handle(File file, String text4Search) throws DispatchException {
        List<ResultItem> resultItemList = new ArrayList<>();
        String[] array = file.getName().split("\\.");
        String extension = "." + array[array.length - 1];
        for (Parser parser : parsers) {
            if (parser.isSupportFile(extension)) {
                resultItemList = parser.parse(file, text4Search);
            }
        }
        return resultItemList;
    }
}
